#!/bin/bash

set -euo pipefail

date

currentdate=`date +%Y%m%d`
yesterday=`date -d 'now - 1day' +%Y%m%d`

echo "Current date is $currentdate, yesterday is $yesterday"

LOCATION="https://data.pid.cz/PID_GTFS.zip"
ETAG=$(curl -L -sq -I "${LOCATION}" | grep ETag)
if [ -f gtfs.zip.etag ] ; then
  OLD_ETAG=$(cat gtfs.zip.etag)
fi
if [ "${OLD_ETAG:-}" != "$ETAG" ] ; then
  echo "Downloading new GTFS from $LOCATION"
  rm -f gtfs.zip
  rm -f gtfs.sqlite
  wget $LOCATION -O gtfs.zip
  echo "$ETAG" > gtfs.zip.etag
fi

docker build --tag gtfs-osm-validator .
docker run -v $(pwd):/data -it gtfs-osm-validator python3 /opt/gtfs-osm-validator/src/main.py --output-sqlite-file /data/gtfs.sqlite --config prague /data/gtfs.zip | tee output-$currentdate.txt
# fix sqlite database ownership
docker run -v $(pwd):/data -it ubuntu:20.04 chown $(id -u):$(id -g) /data/gtfs.sqlite

echo "Done work for today"







