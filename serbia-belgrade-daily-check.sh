#!/bin/bash
set -euo pipefail

date

currentdate=`date +%Y%m%d`
yesterday=`date -d 'now - 1day' +%Y%m%d`

echo "Current date is $currentdate, yesterday is $yesterday"

PERMALINK="https://data.gov.rs/sr/datasets/r/729be9a1-7ed9-453d-9a3d-68fa30f07529"
LOCATION=$(curl -sI $PERMALINK | fgrep -i 'Location:' | sed -e 's/^Location:\s*//i' -e 's/\r$//')
echo "Downloading new GTFS from $LOCATION"
rm -f gtfs.zip
rm -f gtfs.sqlite
wget $LOCATION -O gtfs.zip

python3 src/main.py gtfs.zip > output-$currentdate.txt
echo "Done work for today"

if [ -f "output-$yesterday.txt" ]; then
  diff -u output-$yesterday.txt output-$currentdate.txt > output-$currentdate.diff || true
  echo "Diff done for $currentdate"
  if [ -s output-$currentdate.diff ]; then
    curl -s "https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage?chat_id=$TELEGRAM_CHAT_ID&text=Changes%20for%20GTFS%20Beograd%20for%20$currentdate"
  else
    curl -s "https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage?chat_id=$TELEGRAM_CHAT_ID&text=No%20changes%20for%20GTFS%20Beograd%20for%20$currentdate"
  fi
else
  echo "No file output-$yesterday.txt, no diff done"
  curl -s "https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage?chat_id=$TELEGRAM_CHAT_ID&text=No%20data%20from%yesterday%20for%20GTFS%20Beograd"
fi

